// serenity
"setup.scd".loadRelative;

{SinOsc.ar(330, mul:0.2!2) * Env.perc.kr(2)}.play(target:~src);
{SinOsc.ar(330, mul:0.2!2) * Env.perc.kr(2)}.play(target:~src, outbus:~bus[\reverb]);

// SynthDefs
// square/saw voice
(
SynthDef(\square, {
	arg dry = 0.5;
	var sig1, sig2, sig, env, freq, dt, at, rel, cutenv, gate;

	freq = \freq.kr(220, 0.02).clip(20,20000);
	at = \atk.ir(0.1) * freq.linexp(20, 20000, 2, 0.5);
	rel = \rel.ir(2);
	dt = LFNoise2.kr(\dtfreq.kr(0.2!5)).bipolar(\dtune.kr(0.15, 0.05)).midiratio;
	freq = freq * dt;
	gate = \gate.kr(1);

	env = Env.adsr(at, \dec.ir(0.2), \suslev.ir(1), rel, curve: \curve.kr(-2)).kr(2, gate);
	cutenv = EnvGen.kr(
		Env(
			[0.66, 1, 0.5],
			[at * 2, rel], 
			[\sine, \exp],
			releaseNode: 1
		), gate
	);

	//square
	sig1 = PulseDPW.ar(
		freq,
		\pwidth.kr(0.5),
		mul: 0.1
	).sum;

	// saw
	sig2 = VarSaw.ar(
		freq,
		0,
		\swidth.kr(0.0001),
		mul: 0.1
	).sum;

	sig = sig1.blend(sig2, \blend.kr(0.5, 0.05));
	sig = LPF.ar(sig, \cutoff.kr(3200, 0.075).clip(50, 20000) * cutenv);
	sig = sig * env * \amp.kr(-6.dbamp);
	sig = Pan2.ar(sig, \pan.kr(0));

	Out.ar(\out.ir(0), sig * dry);
	Out.ar(\sendbus.ir(0), sig * \send.kr(0.25));
	Out.ar(\fx.ir(0), sig * (1- dry));
}).add;

SynthDef(\bling, {
	arg dry = 0.5;
	var sig, env, freq, at;
	freq = \freq.kr(220, 0.02).clip(20,20000);
	at = \atk.ir(0.001) * freq.linexp(20, 20000, 2, 0.5);
	env = Env.perc(at, \rel.ir(1.2)).kr(2);
	sig = SinOsc.ar(freq, mul: 0.2);
	sig = Pan2.ar(sig, \pan.kr(0));
	sig = sig * env * \amp.kr(-6.dbamp);
	Out.ar(\out.kr(0), sig * dry);
	Out.ar(\fx.kr(0), sig * (1-dry))
}).add;

SynthDef(\delay, {
	arg dry = 0.5;
	var sig;
	sig = In.ar(\in.kr(0), 2);

	sig = AllpassC.ar(
		sig,
		\maxdel.kr(2),
		\del.kr(0.5),
		\decay.kr(2),
	);

	Out.ar(\out.kr(0), sig * dry);
	Out.ar(\fx.kr(0), sig * (1 - dry));
}).add;
)
~bus.add(\delay -> Bus.audio(s, 2));
(
~delaySynth = Synth(\delay, [
	\dry, 0.5,
	\in, ~bus[\delay],
	\maxdel, 2.0,
	\del, 0.5,
	\decay, 2.0,
	\out, ~out,
	\fx, ~bus[\reverb],
	], ~efx);
)

~delaySynth.set(\decay, 4);
~delaySynth.set(\del, t.tempo);
~delaySynth.free;

(
a = Synth(\square, [
	\freq, (Scale.minorPentatonic.degrees.choose + 51), 
	\amp, -12.dbamp, 
	\fx, ~bus[\reverb], 
	\atk, 0.01, 
	\rel, 0.75, 
	\dec, 0.2, 
	\suslev, 0.1, 
	\blend, 0.1, 
	\pwidth, 0.4, 
	\cutoff, 660, 
	\sendbus, ~bus[\delay], 
	\send, 0.5
], ~src);
)
a.set(\cutoff, 800);
a.set(\gate, 0);

b = Synth(\bling, [\freq, 39.midicps, \dry, 0.5, \fx, ~bus[\reverb]], ~src);

// Patterns

(
// base pattern, common scaffolding for all patterns
~base = Pbind(
	\scale, Scale.minorPentatonic(\mean4),
	\group, ~src, 
	\fx, ~bus[\reverb]
);
)

(
~chordLayer = Pbind(
	\instrument, \square,
	\dur, Pwhite(7.999,8.001),
	\octave, Prand((4..6), inf),
	\degree, Prand((0..5), inf).clump(3),
	\dry, Pmeanrand(0.1, 0.2),
	\atk, Pwhite(2.2, 2.6),
	\dtune, Pmeanrand(0.14, 0.16),
	\suslev, 1.0,
	\rel, Pwhite(3.1, 3.3),
	\curve, [2, -2],
	\pwidth, Pwhite(0.48, 0.52),
	\swidth, Pwhite(0.18, 0.22),
	\blend, Plprand(0.24, 0.3),
	\cutoff, Pmeanrand(1000, 1600),
	\amp, Pwhite(-16.1,-15.9).dbamp,
	\pan, Pwhite(-0.1, 0.1),
	\legato, 0.4
) <> ~base;

~bass = Pbind(
	\instrument, \square,
	\dur, 2, 
	\octave, 4, 
	\degree, Pdup(4, Prand([0, 4], inf)),
	\amp, -12.dbamp, 
	\atk, 0.01, 
	\rel, 0.75, 
	\dec, 0.2, 
	\suslev, 0.1, 
	\blend, 0.1, 
	\pwidth, 0.4, 
	\cutoff, 660, 
	\sendbus, ~bus[\delay], 
	\send, 0.5
) <> ~base;
)

// event players
c = ~chordLayer.play(t, quant:t.beatsPerBar);
d = ~bass.play(t, quant:t.beatsPerBar);
c.stop;
d.stop;

(
~blings = Pbind(
	\instrument, \bling,
	\dur, 1/2,
	\octave, 5, 
	\degree, Prand([
		Pseq([
			Pseq([[7, 0], 3, 0], 3),
			Pseq([\, \, \]),
		]),
		Pseq([
			Pseq([[8, 5], 7, 2], 3),
			Pseq([\, \, \]),
		]),
		Pseq([\, \, \, \, \, \])
	], 56),
	\dry, 0.6,
	\atk, 0.01,
	\rel, 1.2,
	\pan, Pdup(3, Pwhite(-0.75, 0.75)),
	//\amp, Pwhite(0.9, 0.99) * -12.dbamp, 
	\amp, Pseq([-16, -12, -12].dbamp, inf),
	//\legato, Pseq([1.1, 1, 0.8],9)
) <> ~base;
)

d = ~blings.play(t, quant:t.beatsPerBar);
d.stop;

t.tempo_(76/60);
// further ideas:
// develop a more aleatoric alternative to the pitches in blings 
// add a delay effect
// make a very subtle bass line
(
~recRoutine = Routine{
	//var dur = 180; // enter duration in seconds
	~recFNString = PathName(thisProcess.nowExecutingPath).parentPath ++ "recordings\\"++ Date.getDate.format("%Y-%m-%d_%H-%M") ++".wav";

	s.record(~recFNString);
	0.02.wait; // wait long enough to get recording started

	// put stuff to be played here, make sure it will be finished within duration
	c = ~chordLayer.play(t, quant:t.beatsPerBar);
	32.wait;
	d = ~blings.play(t, quant:t.beatsPerBar);

	96.wait;
	c.stop;
	d.stop;
	20.wait;
	s.stopRecording;
};
)
~recRoutine.play;
~recRoutine.stop;
~recRoutine.reset;



s.quit;





